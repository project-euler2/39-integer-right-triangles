import math
import time
start_time = time.time()

def find_triangle_perimeter_max_solutions(limit):
    best_num_solutions = 0
    best_perimeter = 0
    for i in range(2,limit,2):
        num_solutions = 0
        for a in range(1,int(i/2)):
            for b in range (a,int(i/2)):
                product_square = a**2+b**2
                if (math.isqrt(product_square) ** 2 == product_square) and (a+b+int(math.sqrt(product_square))) == i:
                    num_solutions +=1
            if num_solutions > best_num_solutions:
                best_num_solutions = num_solutions
                best_perimeter = i
    return best_perimeter

print(find_triangle_perimeter_max_solutions(1000))
print(f"--- {(time.time() - start_time):.10f} seconds ---" )